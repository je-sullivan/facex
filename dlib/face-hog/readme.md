1.- Install Cmake

    $sudo apt install cmake

2.- Check Cmake's version

    $cmake --version

    The minimum version required is 2.8.12

3.- Download dlib library
    http://dlib.net/

4.- Uncompress into dlib directory. The version used is 19.19

    In face-hog directory

    $mkdir build

    $cd build

    $cmake ..

    $cmake --build . --config Release

5.- To test face detection

    In build directory execute
    
    $./face_detection_ex ../images/tony_stark.jpeg
